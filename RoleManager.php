<?php

namespace Clappo\Roles;

use Closure;
use ReflectionClass;
use ReflectionMethod;
use ReflectionProperty;
use BadMethodCallException;

trait RoleManager {

	public static $methods    = array();
	public static $properties = array();

	/**
	 * Adds a role to an object.
	 *
	 * @return void
	 */
	public static function ROLE__Gain( object $role ) {
        
        $roleReflection = new ReflectionClass( $role );
        
        // Add the methods.
		foreach ( self::_ROLE__Methods( $roleReflection ) as $method ) {
			$method->setAccessible( true );
			self::_ROLE__AddMethodMacro( $method->name, $method->invoke( $role ) );
		}

		// Add properties.
		foreach ( self::_ROLE__Properties( $roleReflection ) as $property ) {
			self::_ROLE__AddPropertiesMacro( $property->name, $roleReflection->getProperty( $property->name )->getValue( new $role() ) );
		}
	}

	/**
	 * Removes a role from an object.
	 *
	 * @return void
	 */
	public static function ROLE__Forget( object $role ) {
		// Unset methods if they exist.
		foreach ( self::_ROLE__Methods( new ReflectionClass( $role ) ) as $method ) {
			if ( self::_ROLE__HasMethodMacro( $method->name ) ) {
				unset( self::$methods[ $method->name ] );
			}
		}

		// Unset propertoes if they exist.
		foreach ( self::_ROLE__Properties( new ReflectionClass( $role ) ) as $property ) {
			if ( self::_ROLE__HasPropertiesMacro( $property->name ) ) {
				unset( self::$properties[ $property->name ] );
			}
		}
	}

	/**
	 * Clears all the roles from an object (methods & properties)
	 *
	 * @return void
	 */
	public static function ROLE__Flush() {
		self::$methods    = array();
		self::$properties = array();
	}

	/** Addings Methods & Properties */

	/**
	 * Add method from role.
	 *
	 * @param  string          $name The name of the added method.
	 * @param  object|callable $macro The closure being added.
	 */
	private static function _ROLE__AddMethodMacro( string $name, $macro ): void {
		self::$methods[ $name ] = $macro;
	}

	/**
	 * Checks if a method has been assigned as a macro..
	 *
	 * @param string $name
	 * @return boolean
	 */
	private static function _ROLE__HasMethodMacro( string $name ): bool {
		return isset( self::$methods[ $name ] );
	}

	/**
	 * Add method from role.
	 *
	 * @param  string $name The name of the property.
	 * @param  mixed  $value The value of the property
	 */
	private static function _ROLE__AddPropertiesMacro( string $name, $value ): void {
		self::$properties[ $name ] = $value;
	}

	/**
	 * Checks if the property has been removed.
	 *
	 * @param string $name
	 * @return boolean
	 */
	private static function _ROLE__HasPropertiesMacro( string $name ): bool {
		return isset( self::$properties[ $name ] );
	}

	/**
	 * Gets an array of all public and protected methods from role.
	 *
	 * @param ReflectionClass $role
	 * @return array
	 */
	private static function _ROLE__Methods( ReflectionClass $role ): array {
		return $role->getMethods(
			ReflectionMethod::IS_PUBLIC | ReflectionMethod::IS_PROTECTED
		);
	}

	/**
	 * Gets an array of all public and protected properties from the role.
	 *
	 * @param ReflectionClass $role
	 * @return array
	 */
	private static function _ROLE__Properties( ReflectionClass $role ): array {
		return $role->getProperties(
			ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED
		);
	}

	/** Magic Methods */

	/**
	 * Call role closure as a static method.
	 *
	 * @param string $method Method to call.
	 * @param array  $parameters Any parameters to pass.
	 * @return mixed
	 */
	public static function __callStatic( string $method, ?array $parameters ) {
		if ( ! self::_ROLE__HasMethodMacro( $method ) ) {
			throw new BadMethodCallException( "Method {$method} does not exist." );
		}

		$macro = self::$methods[ $method ];

		if ( $macro instanceof Closure ) {
			return call_user_func_array( Closure::bind( $macro, null, self::class ), $parameters );
		}

		return call_user_func_array( $macro, $parameters );
	}

	/**
	 * Call role closure as a public method.
	 *
	 * @param string $method Method to call.
	 * @param array  $parameters Any parameters to pass.
	 * @return mixed
	 */
	public function __call( $method, $parameters ) {
		if ( ! self::_ROLE__HasMethodMacro( $method ) ) {
			throw new BadMethodCallException( "Method {$method} does not exist." );
		}

		$macro = self::$methods[ $method ];

		if ( $macro instanceof Closure ) {
			return call_user_func_array( $macro->bindTo( $this, self::class ), $parameters );
		}

		return call_user_func_array( $macro, $parameters );
	}

	/**
	 * Allows the getting of role property.
	 *
	 * @param string $property
	 * @return void
	 */
	public function __get( $property ) {
		if ( self::_ROLE__HasPropertiesMacro( $property ) ) {
			return self::$properties[ $property ];
		}
		return null;
	}

	/**
	 * Allows the getting of role property.
	 *
	 * @param string $property
	 * @param mixed  $value
	 * @return void
	 */
	public function __set( $property, $value ) {
		if ( self::_ROLE__HasPropertiesMacro( $property ) ) {
			self::$properties[ $property ] = $value;
		}
	}
}
